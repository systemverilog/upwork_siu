module square_ROM_tb;

reg clk, rst;

//DUT signals
reg	[3:0]	n;
reg 		sign;
wire [7:0]	square;

//tb signals
integer		i;
reg signed [3:0]	n_signed;
reg [7:0]			tb_square;
wire				err_flag;

square_ROM 	square_ROM_inst(.n(n), .sign(sign), .square(square));


initial // Clock generator
  begin
    clk = 0;
    #10 forever #10 clk = !clk;
  end
  
initial	// Test stimulus
  begin
    rst = 0;
    #5 rst = 1;
    #4 rst = 0;
	n_signed = 0;
	
	//test unsigned
	$display("Test unsigned!");
	#10 sign = 0;  
	for (i=0; i < 16; i=i+1) begin
		n = i;
		tb_square = n*n;
		@(posedge clk);
	end
	
	//test signed
	$display("Test signed!");
	sign = 1;
	for (i=0; i < 16; i=i+1) begin
		n = i;
		n_signed = i;
		tb_square = n_signed*n_signed;
		@(posedge clk);
	end
	
	//delay before stop
	repeat(10)
		@(posedge clk);
    
    $stop;
  end

assign err_flag = (square != tb_square);

initial
    $monitor("Time: %3d - n=%d, sign=%d, square=%d \n", $time, n, sign, square); 
    
endmodule