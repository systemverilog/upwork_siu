module square_ROM (n, sign, square);
input [3:0] n ; // signed number.
input sign ; // signed number if it is high and unsigned otherwise.
output [7:0] square ; // Result = n x n.
reg [7:0] square ;

always @ (n or sign) begin
	if (sign == 0) // Output "square" if the input is unsigned.
		begin
			case (n)
				0 : square 	<= 8'd0 ;
				1 : square 	<= 8'd1 ;
				2 : square 	<= 8'd4 ;
				3 : square 	<= 8'd9 ;
				4 : square 	<= 8'd16 ;
				5 : square 	<= 8'd25 ;
				6 : square 	<= 8'd36 ;
				7 : square 	<= 8'd49 ;
				8 : square 	<= 8'd64 ;
				9 : square 	<= 8'd81 ;
				10 : square <= 8'd100 ;
				11 : square <= 8'd121 ;
				12 : square <= 8'd144 ;
				13 : square <= 8'd169 ;
				14 : square <= 8'd196 ;
				15 : square <= 8'd225 ;
				default : square <= 0 ; // Clear the result.
			endcase
	 	end
	else
	 	begin
			case (n) // Output "square" if the input is unsigned.
				0 : square 	<= 8'd0 ;
				1 : square 	<= 8'd1 ;
				2 : square 	<= 8'd4 ;
				3 : square 	<= 8'd9 ;
				4 : square 	<= 8'd16 ;
				5 : square 	<= 8'd25 ;
				6 : square 	<= 8'd36 ;
				7 : square 	<= 8'd49 ;
				8 : square 	<= 8'd64 ;	//-8
				9 : square 	<= 8'd49 ;	//-7
				10 : square <= 8'd36 ;	//-6
				11 : square <= 8'd25 ;	//-5
				12 : square <= 8'd16 ;	//-4
				13 : square <= 8'd9 ;	//-3
				14 : square <= 8'd4 ;	//-2
				15 : square <= 8'd1 ;	//-1
				default : square <= 0 ; // Clear the result.
	 		endcase
	 	end
end
endmodule
