# create library
if [file exists work] {
    vdel -all
}
vlib work
 
# compile all source files
vlog square_ROM.v square_ROM_tb.v

#optimize design
vopt +acc square_ROM_tb -o square_ROM_tb_opt

# load simulator with optimized design
vsim square_ROM_tb_opt

# wave signals
add wave /square_ROM_tb/*
add wave /square_ROM_tb/square_ROM_inst/*
add log -r /*

