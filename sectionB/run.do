# create library
if [file exists work] {
    vdel -all
}
vlib work
 
# compile all source files
vlog square_root_ROM.v square_root_ROM_tb.v

#optimize design
vopt +acc square_root_ROM_tb -o square_root_ROM_tb_opt

# load simulator with optimized design
vsim square_root_ROM_tb_opt

# wave signals
add wave /square_root_ROM_tb/*
add wave /square_root_ROM_tb/square_root_ROM_inst/*
add log -r /*

