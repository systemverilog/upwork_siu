module square_root_ROM_tb;

reg clk, rst;

//DUT signals
reg	[3:0]	n;
wire [7:0]	sq_root;

//tb signals
integer		i;

square_root_ROM 	square_root_ROM_inst (.n(n), .sq_root(sq_root));

initial // Clock generator
  begin
    clk = 0;
    #10 forever #10 clk = !clk;
  end
  
initial	// Test stimulus
  begin
    rst = 0;
    #5 rst = 1;
    #4 rst = 0;
	
	//test start
	$display("Test start!");
	for (i=0; i < 16; i=i+1) begin
		n = i;
		@(posedge clk);
	end
	
	//delay before stop
	repeat(10)
		@(posedge clk);
    
    $stop;
  end

initial
    $monitor("Time: %3d - n=%b, sq_root=%b \n", $time, n, sq_root); 
    
endmodule